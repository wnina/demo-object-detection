from flask import Flask, render_template
from flask_cors import CORS, cross_origin

app = Flask(__name__)

@app.route('/')
@cross_origin() # allow all origins all methods.
def index():
    return render_template('index.html')

if __name__ == "__main__":
    #app.run(ssl_context='adhoc', host="0.0.0.0", port=5000, debug=True)
    #app.run(ssl_context=('cert.pem', 'key.pem'), host="0.0.0.0", port=5000, debug=True)
    app.run(host="0.0.0.0", port=5000, debug=True)
