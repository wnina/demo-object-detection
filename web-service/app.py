from flask import Flask, jsonify, request
from yolo import get_predictions
from flask_cors import CORS, cross_origin


app = Flask(__name__)

@app.route("/predict", methods=["POST"])
@cross_origin() # allow all origins all methods.
def predict():
    #image = request.files["file"]
    #print("values", request.values)
    #print("form", request.form['image'])
    image = request.files.get('image')
    #print("files", request.files.get('image'))
    predictions = get_predictions(image)
    return jsonify(predictions)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5001, debug=True)
    #app.run(ssl_context='adhoc', host="0.0.0.0", port=5001, debug=True)
    #app.run(ssl_context=('cert.pem', 'key.pem'), host="0.0.0.0", port=5001, debug=True)
